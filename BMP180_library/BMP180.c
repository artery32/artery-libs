/**
  ******************************************************************************

  BMP180 LIBRARY for STM32 using I2C
  Author:   ControllersTech
  Updated:  26/07/2020

  ******************************************************************************
  Copyright (C) 2017 ControllersTech.com

  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.

  ******************************************************************************
*/

#include "BMP180.h"
#include "at32f403a_407.h"
#include "math.h"
//#include "at32f403a_407.h" //Подключение библиотеки к определённому микроконтроллеру. В данном случае к AT32F403AVGT7.
//#include "i2c_application.h"
//#include "math.h"

extern i2c_handle_type hi2c1;

#define BMP180_I2C &hi2c1

#define BMP180_ADDRESS 0xEE

//#define STEP_DELAY_MS 1


// Defines according to the datasheet
short AC1 = 0;
short AC2 = 0;
short AC3 = 0;
unsigned short AC4 = 0;
unsigned short AC5 = 0;
unsigned short AC6 = 0;
short B1 = 0;
short B2 = 0;
short MB = 0;
short MC = 0;
short MD = 0;

/********************/
long UT = 0;
short oss = 0;
long UP = 0;
long X1 = 0;
long X2 = 0;
long X3 = 0;
long B3 = 0;
long B5 = 0;
unsigned long B4 = 0;
long B6 = 0;
unsigned long B7 = 0;

/*******************/
long Press = 0;
long Temp = 0;

/* delay variable */
//static __IO uint32_t fac_us;
//static __IO uint32_t fac_ms;

#define atmPress 101325 //Pa

//void delay_init()
//{
//  /* configure systick */
//  systick_clock_source_config(SYSTICK_CLOCK_SOURCE_AHBCLK_NODIV);
//  fac_us = system_core_clock / (1000000U);
//  fac_ms = fac_us * (1000U);
//
//  //systick_clock_source_config(SYSTICK_CLOCK_SOURCE_AHBCLK_DIV8);
//  //fac_us = system_core_clock / (8000000U);
//  //fac_ms = fac_us * (8000U);
//}
//
//void delay_us(uint32_t nus)
//{
//  uint32_t temp = 0;
//  SysTick->LOAD = (uint32_t)(nus * fac_us);
//  SysTick->VAL = 0x00;
//  SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk ;
//  do
//  {
//    temp = SysTick->CTRL;
//  }while((temp & 0x01) && !(temp & (1 << 16)));
//
//  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
//  SysTick->VAL = 0x00;
//}
//
///**
//  * @brief  inserts a delay time.
//  * @param  nms: specifies the delay time length, in milliseconds.
//  * @retval none
//  */
//void delay_ms(uint16_t nms)
//{
//  uint32_t temp = 0;
//  while(nms)
//  {
//    if(nms > STEP_DELAY_MS)
//    {
//      SysTick->LOAD = (uint32_t)(STEP_DELAY_MS * fac_ms);
//      nms -= STEP_DELAY_MS;
//    }
//    else
//    {
//      SysTick->LOAD = (uint32_t)(nms * fac_ms);
//      nms = 0;
//    }
//    SysTick->VAL = 0x00;
//    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
//    do
//    {
//      temp = SysTick->CTRL;
//    }while((temp & 0x01) && !(temp & (1 << 16)));
//
//    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
//    SysTick->VAL = 0x00;
//  }
//}

void read_calliberation_data (void)
{
	uint8_t Callib_Data[22] = {0};
	uint16_t Callib_Start = 0xAA;
	//i2c_memory_read(BMP180_I2C, BMP180_ADDRESS, Callib_Start, 1, Callib_Data,22, 0xFFFFFFFF);
	i2c_memory_read(BMP180_I2C, I2C_MEM_ADDR_WIDIH_8, BMP180_ADDRESS, Callib_Start, Callib_Data, 22, 0xFFFFFFFF);

	AC1 = ((Callib_Data[0] << 8) | Callib_Data[1]);
	AC2 = ((Callib_Data[2] << 8) | Callib_Data[3]);
	AC3 = ((Callib_Data[4] << 8) | Callib_Data[5]);
	AC4 = ((Callib_Data[6] << 8) | Callib_Data[7]);
	AC5 = ((Callib_Data[8] << 8) | Callib_Data[9]);
	AC6 = ((Callib_Data[10] << 8) | Callib_Data[11]);
	B1 = ((Callib_Data[12] << 8) | Callib_Data[13]);
	B2 = ((Callib_Data[14] << 8) | Callib_Data[15]);
	MB = ((Callib_Data[16] << 8) | Callib_Data[17]);
	MC = ((Callib_Data[18] << 8) | Callib_Data[19]);
	MD = ((Callib_Data[20] << 8) | Callib_Data[21]);

}


// Get uncompensated Temp
uint16_t Get_UTemp (void)
{
	uint8_t datatowrite = 0x2E;
	uint8_t Temp_RAW[2] = {0};
	i2c_memory_write(BMP180_I2C, BMP180_ADDRESS, 0xF4, 1, &datatowrite, 1, 1000);
	delay_ms(5);  // wait 4.5 ms
	i2c_memory_read(BMP180_I2C, BMP180_ADDRESS, 0xF6, 1, Temp_RAW, 2, 1000);
	return ((Temp_RAW[0]<<8) + Temp_RAW[1]);
}

float BMP180_GetTemp (void)
{
	UT = Get_UTemp();
	X1 = ((UT-AC6) * (AC5/(pow(2,15))));
	X2 = ((MC*(pow(2,11))) / (X1+MD));
	B5 = X1+X2;
	Temp = (B5+8)/(pow(2,4));
	return Temp/10.0;
}

// Get uncompensated Pressure
uint32_t Get_UPress (int oss)   // oversampling setting 0,1,2,3
{
	uint8_t datatowrite = 0x34+(oss<<6);
	uint8_t Press_RAW[3] = {0};
	i2c_memory_write(BMP180_I2C, BMP180_ADDRESS, 0xF4, 1, &datatowrite, 1, 1000);
	switch (oss)
	{
		case (0):
			delay_ms(5);
			break;
		case (1):
			delay_ms(8);
			break;
		case (2):
			delay_ms(14);
			break;
		case (3):
			delay_ms(26);
			break;
	}
	i2c_memory_read(BMP180_I2C, BMP180_ADDRESS, 0xF6, 1, Press_RAW, 3, 1000);
	return (((Press_RAW[0]<<16)+(Press_RAW[1]<<8)+Press_RAW[2]) >> (8-oss));
}


float BMP180_GetPress (int oss)
{
	UP = Get_UPress(oss);
	X1 = ((UT-AC6) * (AC5/(pow(2,15))));
	X2 = ((MC*(pow(2,11))) / (X1+MD));
	B5 = X1+X2;
	B6 = B5-4000;
	X1 = (B2 * (B6*B6/(pow(2,12))))/(pow(2,11));
	X2 = AC2*B6/(pow(2,11));
	X3 = X1+X2;
	B3 = (((AC1*4+X3)<<oss)+2)/4;
	X1 = AC3*B6/pow(2,13);
	X2 = (B1 * (B6*B6/(pow(2,12))))/(pow(2,16));
	X3 = ((X1+X2)+2)/pow(2,2);
	B4 = AC4*(unsigned long)(X3+32768)/(pow(2,15));
	B7 = ((unsigned long)UP-B3)*(50000>>oss);
	if (B7<0x80000000) Press = (B7*2)/B4;
	else Press = (B7/B4)*2;
	X1 = (Press/(pow(2,8)))*(Press/(pow(2,8)));
	X1 = (X1*3038)/(pow(2,16));
	X2 = (-7357*Press)/(pow(2,16));
	Press = Press + (X1+X2+3791)/(pow(2,4));

	return Press;
}


float BMP180_GetAlt (int oss)
{
	BMP180_GetPress(oss);
	return 44330*(1-(pow((Press/(float)atmPress), 0.19029495718)));
}

void BMP180_Start (void)
{
	read_calliberation_data();
}

