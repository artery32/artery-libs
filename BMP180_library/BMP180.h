/**
  ******************************************************************************

  BMP180 LIBRARY for STM32 using I2C
  Author:   ControllersTech
  Updated:  26/07/2020

  ******************************************************************************
  Copyright (C) 2017 ControllersTech.com

  This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  of the GNU General Public License version 3 as published by the Free Software Foundation.
  This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  or indirectly by this software, read more about this on the GNU General Public License.

  ******************************************************************************
*/

#ifndef _BMP180_H_
#define _BMP180_H_

#include "i2c_application.h"
#include "at32f403a_407.h"
#include "at32f403a_407_wk_config.h"
#include "math.h"

void BMP180_Start (void);

//void delay_init(void);
//
//void delay_us(uint32_t nus);
//
//void delay_ms(uint16_t nms);

float BMP180_GetTemp (void);

float BMP180_GetPress (int oss);

float BMP180_GetAlt (int oss);

#endif /* INC_BMP180_H_ */
