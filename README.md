# Artery-libs

Библиотеки для микроконтроллеров Artery

Libraries for microcontrollers Artery

## Описание (Description)

Сюда будут выкладываться различные библиотеки под различные переферийные устройства для дальнейшего подключения к микроконтроллерам семейства Artery.
Некоторые библиотеки являются портированными версиями библиотек переферийных устройств для их подключения к микроконтроллерам семейства STM32.

Various libraries for various peripheral devices will be laid out here for further connection to the microcontrollers of the Artery.
Some libraries are ported versions of peripheral device libraries for their connection to STM32.

## Требования (Requirements)

Для устройств, передающих данные по шине I2C, в проекте необходимо подключить прикладную библиотеку работы с шиной I2C. Находятся в общих библиотеках (Firmware Library) к определённому микроконтроллеру семейства Artery в папке middlewares/i2c_application_library.

For devices transmitting data over the I2C bus, the I2C bus application library must be connected in the project. They are located in the shared libraries (Firmware Library) for a specific microcontroller of the Artery family in the middlewares/i2c_application_library folder.
